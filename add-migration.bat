REM AddMigration.bat MigrationName
REM Creates migration entries for defined databases

dotnet ef migrations add %1 --context=SqliteContext --output-dir=Migrations/SqliteMigrations
REM dotnet ef migrations add %1 --context=PostgresContext --output-dir=Migrations/PostgresMigrations
REM dotnet ef migrations add %1 --context=SqlServerContext --output-dir=Migrations/SqlServer
