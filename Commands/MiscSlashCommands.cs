﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.SlashCommands;
using DSharpPlus.CommandsNext.Attributes;
using Serilog;
using System.Text.RegularExpressions;

namespace RoleBringer.Commands
{
    class MiscSlashCommands : ApplicationCommandModule
    {
        [SlashCommand("about", "Information about this bot.")]
        public async Task About(InteractionContext ctx)
        {
            var embed = new DiscordEmbedBuilder()
                .WithTitle("About RoleBringer")
                .WithUrl("https://gitlab.com/the-skwad/rolebringer")
                .WithColor(DiscordColor.CornflowerBlue)
                .WithDescription("RoleBringer is a minimalistic discord bot for enabling roles in a server to be joined by any of its members.  " +
                    "It was designed for The Skwad, a FFXIV Free Company, to use in their discord.\n  " +
                    "Project page: https://gitlab.com/the-skwad/rolebringer")
                .AddField("Public Commands", "```\nadd-my-role\nlist-roles\nremove-myrole\n```", true)
                .AddField("Admin Commands", "```\nallow-role\ndisallow-role\n```", true);
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder()
                    .AsEphemeral(true)
                    .AddEmbed(embed));
        }

    }
}
