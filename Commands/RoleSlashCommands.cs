﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using Serilog;
using System.Text.RegularExpressions;

namespace RoleBringer.Commands
{
    class RoleSlashCommands : ApplicationCommandModule
    {
        [SlashCommand("add", "Adds the specified public 'role' to your user.")]
        public async Task AddRoleToUser(
            InteractionContext ctx,
            [Option("role", "Name of requested role")]
            string RoleName)
        {
            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.IndianRed); // assume error.
            if (ctx.Channel.IsPrivate)
            {
                await SendPrivateFailureResponse(ctx);
                return;
            }
            DiscordRole role = await FindRoleFromText(ctx, RoleName);
            if (role != null)
            {
                DiscordMember member = await ctx.Guild.GetMemberAsync(ctx.User.Id);
                using (var db = DB.RBDataContext.GetNewContext())
                {
                    var foundRole = db.GuildRoles
                        .Where(gr => gr.DiscordGuildID == ctx.Guild.Id && gr.DiscordRoleID == role.Id)
                        .FirstOrDefault();
                    if (foundRole != null)
                    {
                        Log.Debug($"AddRoleToUser: adding role: {RoleName}");
                        try
                        {
                            await member.GrantRoleAsync(role);
                            embed = embed
                                .WithColor(DiscordColor.SapGreen)
                                .WithDescription($"Added the role `{role.Name}` to you here in **{ctx.Guild.Name}**!");
                        }
                        catch (DSharpPlus.Exceptions.UnauthorizedException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Problem adding role `{role.Name}` to you here in **{ctx.Guild.Name}**, I apparently am not authorized to do so!");
                        }
                        catch (DSharpPlus.Exceptions.NotFoundException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Role {RoleName} not found.  Which doesn't make sense, since I checked a moment ago for it!");
                        }
                        catch (DSharpPlus.Exceptions.BadRequestException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Failed to apply role.  BadRequestException encountered.");
                        }
                        catch (DSharpPlus.Exceptions.ServerErrorException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Failed to apply role.  ServerErrorException encountered.");
                        }
                    }
                    else
                    {
                        // Role exists, but has not been made public.
                        Log.Debug($"AddRoleToUser: Not adding private role: {RoleName}");
                        embed = embed.WithDescription($"`{role.Name}` is not a role I can assign in **{ctx.Guild.Name}**.  Use `/list-roles` to get the list of available roles.");
                    }
                }
            }
            else
            {
                Log.Debug($"AddRoleToUser: Failed to find role: {RoleName}");
                embed = embed.WithDescription($"Failed to find `{RoleName}` in **{ctx.Guild.Name}**.\nUse `/list-roles` to get the list of available roles.");
            }
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder().AsEphemeral(true).AddEmbed(embed));
        }

        [SlashCommand("allow-role", "Enable admins to allow a role to be available to users in this server")]
        public async Task EnableRoleTousers(
            InteractionContext ctx,
            [Option("role", "Name of role to make publicly available")]
            string RoleName)
        {
            if (ctx.Channel.IsPrivate)
            {
                await SendPrivateFailureResponse(ctx);
                return;
            }

            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.IndianRed); // default red for errors;
            if (PermissionMethods.HasPermission(ctx.Member.Permissions, Permissions.Administrator))
            {
                Log.Debug($"Member {ctx.Member.Username} attempting to add '{RoleName}' to {ctx.Guild.Name}");
                DiscordRole role = await FindRoleFromText(ctx, RoleName);
                if (role != null)
                {
                    Log.Debug($"Adding role {role.Name} for {ctx.Guild.Name}");
                    try
                    {
                        using (var db = DB.RBDataContext.GetNewContext())
                        {
                            var gr = new DB.Model.GuildRole();
                            gr.DiscordGuildID = ctx.Guild.Id;
                            gr.DiscordRoleID = role.Id;
                            db.GuildRoles.Add(gr);
                            db.SaveChanges();
                        }
                        embed = embed
                            .WithColor(DiscordColor.SapGreen)
                            .WithDescription($"Enabling `{role.Name}` as a public role for users in **{ctx.Guild.Name}**");
                    }
                    catch (DbUpdateException exc)
                    {
                        if (exc.InnerException.Message.ToUpper().Contains("UNIQUE CONSTRAINT"))
                        {
                            Log.Warning($"Attempted to allow role {role.Id} ({role.Name}) to Server ({ctx.Guild.Id}/{ctx.Guild.Name}) multiple times.");
                            embed = embed
                                .WithColor(DiscordColor.SapGreen)
                                .WithDescription($"`{role.Name}` is already a public role for users in **{ctx.Guild.Name}**");
                        }
                        else
                        {
                            Log.Error("DbUpdateException: " + exc.ToString());
                        }
                    }
                }
                else
                {
                    Log.Debug($"Failed to find role {RoleName} in {ctx.Guild.Id}/{ctx.Guild.Name}?");
                    embed = embed.WithDescription($"Role `{RoleName}` was not found in **{ctx.Guild.Name}**");
                }
            }
            else
            {
                Log.Warning($"{ctx.Member.Id}/{ctx.Member.Username}#{ctx.Member.Discriminator} " +
                    $"does not have permissions to allow roles on " +
                    $"{ctx.Guild.Id}/{ctx.Guild.Name} ({ctx.Channel.Id}/{ctx.Channel.Name})");
                embed = embed.WithDescription($"You do not have permission to manage {ctx.Guild.Name}");
            }
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder().AsEphemeral(true).AddEmbed(embed));
        }

        [SlashCommand("disallow-role", "Enable admins to remove a role from being available to users in this guild")]
        public async Task DisableRoleToUsers(
            InteractionContext ctx,
            [Option("role", "Name of role to remove from being available")]
            string RoleName)
        {
            if (ctx.Channel.IsPrivate)
            {
                await SendPrivateFailureResponse(ctx);
                return;
            }
            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.IndianRed); // default red for errors;
            if (PermissionMethods.HasPermission(ctx.Member.Permissions, Permissions.Administrator))
            {
                Log.Debug($"Member {ctx.Member.Username} attempting to remove '{RoleName}' to {ctx.Guild.Name}");
                DiscordRole role = await FindRoleFromText(ctx, RoleName);
                if (role != null)
                {
                    Log.Debug($"Removing role {role.Name} for {ctx.Guild.Name}");
                    try
                    {
                        using (var db = DB.RBDataContext.GetNewContext())
                        {
                            var foundRole = db.GuildRoles
                                .Where(gr => gr.DiscordGuildID == ctx.Guild.Id && gr.DiscordRoleID == role.Id)
                                .FirstOrDefault();
                            if (foundRole != null)
                            {
                                db.GuildRoles.Remove(foundRole);
                                db.SaveChanges();
                                embed = embed
                                    .WithColor(DiscordColor.SapGreen)
                                    .WithDescription($"Role `{role.Name}` removed from being publicly used.");
                            }
                            else
                            {
                                embed = embed.WithDescription($"Role `{role.Name}` is not currently allowed.");
                            }
                        }
                    }
                    catch (DbUpdateException exc)
                    {
                        Log.Error("DbUpdateException: " + exc.ToString());
                        embed = embed.WithDescription($"Internal error; contact developer.");
                    }
                }
                else
                {
                    Log.Debug($"Failed to find role?");
                    embed = embed.WithDescription($"Role `{RoleName}` not found in this server.");
                }

            }
            else
            {
                Log.Warning($"{ctx.Member.Id}/{ctx.Member.Username}#{ctx.Member.Discriminator} " +
                    $"does not have permissions to allow roles on " +
                    $"{ctx.Guild.Id}/{ctx.Guild.Name} ({ctx.Channel.Id}/{ctx.Channel.Name})");
                embed = embed.WithDescription($"You do not have permission to manage {ctx.Guild.Name}");
            }
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder().AsEphemeral(true).AddEmbed(embed));
        }

        [SlashCommand("list-members", "List the members of the selected role")]
        public async Task ListMembers(
            InteractionContext ctx,
            [Option("role", "Name of the role for which to display members")]
            string RoleName)
        {
            if (ctx.Channel.IsPrivate)
            {
                await SendPrivateFailureResponse(ctx);
                return;
            }
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource, new DiscordInteractionResponseBuilder().AsEphemeral(true));
            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.IndianRed); // default red for errors;
            DiscordRole role = await FindRoleFromText(ctx, RoleName);
            if(role == null)
            {
                embed = embed.WithDescription($"Failed to find any role named '{RoleName}'");
            }
            else
            {
                using (var db = DB.RBDataContext.GetNewContext())
                {
                    var guildrole = db.GuildRoles
                        .Where(gr => gr.DiscordRoleID == role.Id)
                        .FirstOrDefault();
                    if(guildrole != null)
                    {
                        var roleMembers = ctx.Guild.Members
                            .Where(m => m.Value.Roles.Contains(role))
                            .Select(m => m.Value.DisplayName)
                            .ToList();
                        if(roleMembers.Count > 9)
                        {
                            double count = (double) roleMembers.Count;
                            int split = (int) Math.Ceiling(count / 3.0);
                            embed = embed
                                .WithColor(DiscordColor.SapGreen)
                                .AddField("Members:", $"```\n" + string.Join("\n", roleMembers.Take(split)) + "\n```", true)
                                .AddField("Members (Cont):", $"```\n" + string.Join("\n", roleMembers.Skip(split).Take(split)) + "\n```", true)
                                .AddField("Members (Cont):", $"```\n" + string.Join("\n", roleMembers.Skip(split * 2)) + "\n```", true);
                        }
                        else
                        {
                            embed = embed
                                .WithColor(DiscordColor.SapGreen)
                                .AddField("Members:", $"```\n" + string.Join("\n", roleMembers) + "\n```");
                        }
                    }
                    else
                    {
                        embed = embed.WithDescription("Selected role is not public, and I can't report on it.");
                    }
                }
            }
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
        }

        [SlashCommand("list-roles", "List the publicly available roles for this server")]
        public async Task ListRoles(InteractionContext ctx)
        {
            if (ctx.Channel.IsPrivate)
            {
                await SendPrivateFailureResponse(ctx);
                return;
            }
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource, new DiscordInteractionResponseBuilder().AsEphemeral(true));
            var embed = new DiscordEmbedBuilder();
            using (var db = DB.RBDataContext.GetNewContext())
            {
                var guildroles = db.GuildRoles
                    .Where(gr => gr.DiscordGuildID == ctx.Guild.Id)
                    .ToList();
                if (guildroles.Count > 0)
                {
                    Dictionary<ulong, int> rolecount = guildroles.ToDictionary(
                        q => q.DiscordRoleID,
                        r => MemberCountInRoleId(ctx.Guild, r.DiscordRoleID));
                    var roles = guildroles.Select(gr => ctx.Guild.GetRole(gr.DiscordRoleID)).ToList();
                    var roleOutput = roles.Select(r => $"{r.Name} ({rolecount[r.Id]})").ToList();
                    embed = embed
                        .WithColor(DiscordColor.CornflowerBlue)
                        .WithDescription($"Available public roles in **{ctx.Guild.Name}** (one per line)")
                        .AddField("Roles", "```\n" + string.Join("\n", roleOutput) + "\n```");
                }
                else
                {
                    embed = embed
                        .WithColor(DiscordColor.IndianRed)
                        .WithDescription($"No roles have been made available in **{ctx.Guild.Name}**");
                }
            }
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
        }

        private static int MemberCountInRoleId(DiscordGuild guild, ulong roleId)
        {
            return guild.Members.Where(m => m.Value.Roles.Where(r => r.Id == roleId).ToList().Count > 0).ToList().Count;
        }

        [SlashCommand("remove", "Removes the specified 'role' to your user")]
        public async Task RemoveRoleFromUser(
            InteractionContext ctx,
            [Option("role", "Name of role to remove from being available")]
            string RoleName)
        {
            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.IndianRed); // default to red (error)
            if (ctx.Channel.IsPrivate)
            {
                await SendPrivateFailureResponse(ctx);
                return;
            }
            DiscordRole role = await FindRoleFromText(ctx, RoleName);
            if (role != null)
            {
                DiscordMember member = await ctx.Guild.GetMemberAsync(ctx.User.Id);
                using (var db = DB.RBDataContext.GetNewContext())
                {
                    var foundRole = db.GuildRoles
                        .Where(gr => gr.DiscordGuildID == ctx.Guild.Id && gr.DiscordRoleID == role.Id)
                        .FirstOrDefault();
                    if (foundRole != null)
                    {
                        try
                        {
                            await member.RevokeRoleAsync(role);
                            embed = embed
                                .WithColor(DiscordColor.SapGreen)
                                .WithDescription($"Removed the role `{role.Name}` from you here in **{ctx.Guild.Name}**!");
                        }
                        catch (DSharpPlus.Exceptions.UnauthorizedException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Problem removing role `{role.Name}`, I apparently am not authorized to do so!");
                        }
                        catch (DSharpPlus.Exceptions.NotFoundException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Role {RoleName} not found.  Which doesn't make sense, since I checked a moment ago for it!");
                        }
                        catch (DSharpPlus.Exceptions.BadRequestException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Failed to apply role.  BadRequestException encountered.");
                        }
                        catch (DSharpPlus.Exceptions.ServerErrorException exc)
                        {
                            Log.Error(exc.ToString());
                            embed = embed.WithDescription($"Failed to apply role.  ServerErrorException encountered.");
                        }
                    }
                    else
                    {
                        embed = embed
                            .WithDescription($"Failed to find \"{RoleName}\" as a publicly available role.");
                    }
                }
            }
            else
            {
                embed = embed
                    .WithDescription($"Failed to find \"{RoleName}\" as a publicly available role.");
            }
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder().AsEphemeral(true).AddEmbed(embed));
        }

        private async Task SendPrivateFailureResponse(InteractionContext ctx)
        {
            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.IndianRed)
                .WithDescription($"I cannot do that via DM.");
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder().AddEmbed(embed));
        }

        private async Task<DiscordRole> FindRoleFromText(InteractionContext ctx, string RoleName)
        {
            var regex = new Regex(@"<@&(?<id>\d+)>");
            Match match = regex.Match(RoleName.Trim());
            if (match.Success && match.Groups.Count > 0)
            {
                Log.Debug("Regex matched!: ");
                Log.Debug($"\t{match.Groups["id"].Value}");
                return ctx.Guild.GetRole(ulong.Parse(match.Groups["id"].Value));
            }
            Log.Debug("Failed to match regex.");
            var roleList = ctx.Guild.Roles;
            return roleList.Where(kv => kv.Value.Name.ToLower() == RoleName.Trim().ToLower()).FirstOrDefault().Value;
        }
    }
}

