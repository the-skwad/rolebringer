# RoleBringer
RoleBringer is a Discord bot written for the FFXIV free company The SKWAD, initially to be used to allow certain roles to be public to its members, and allow them to add those roles to themselves at any time.

# Using on Discord
RoleBringer is invitable to your discord server!  
Here's the link that enables it to accept slash commands as well as assign roles to players:  
https://discord.com/api/oauth2/authorize?client_id=917068472800927805&permissions=268435456&scope=bot%20applications.commands  

It ends up doing two different approval parts:  
1- approves for use as a bot that can Manage Roles (required for assigning roles to users); and  
2- approves for using "application.commands" (slash commands in your guild).

# Running your own copy
Build, copy `appsettings.json` to `appsettings.production.json` and modify the new file.  (This helps to keep one from accidentally overwriting their settings file when deploying a new release.)
