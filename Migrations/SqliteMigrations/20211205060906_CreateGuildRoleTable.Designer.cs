﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RoleBringer.DB;

namespace RoleBringer.Migrations.SqliteMigrations
{
    [DbContext(typeof(SqliteContext))]
    [Migration("20211205060906_CreateGuildRoleTable")]
    partial class CreateGuildRoleTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.12");

            modelBuilder.Entity("RoleBringer.DB.Model.GuildRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<ulong>("DiscordGuildID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("discord_guild_id");

                    b.Property<ulong>("DiscordRoleID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("discord_role_id");

                    b.HasKey("Id")
                        .HasName("pk_guild_roles");

                    b.HasIndex("DiscordGuildID")
                        .HasDatabaseName("ix_guild_roles_discord_guild_id");

                    b.HasIndex("DiscordRoleID")
                        .HasDatabaseName("ix_guild_roles_discord_role_id");

                    b.HasIndex("DiscordGuildID", "DiscordRoleID")
                        .IsUnique()
                        .HasDatabaseName("ix_guild_roles_discord_guild_id_discord_role_id");

                    b.ToTable("guild_roles");
                });
#pragma warning restore 612, 618
        }
    }
}
