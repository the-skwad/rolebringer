﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RoleBringer.Migrations.SqliteMigrations
{
    public partial class CreateGuildRoleTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "guild_roles",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    discord_guild_id = table.Column<ulong>(type: "INTEGER", nullable: false),
                    discord_role_id = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_guild_roles", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_guild_roles_discord_guild_id",
                table: "guild_roles",
                column: "discord_guild_id");

            migrationBuilder.CreateIndex(
                name: "ix_guild_roles_discord_guild_id_discord_role_id",
                table: "guild_roles",
                columns: new[] { "discord_guild_id", "discord_role_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_guild_roles_discord_role_id",
                table: "guild_roles",
                column: "discord_role_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "guild_roles");
        }
    }
}
