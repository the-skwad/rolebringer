﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RoleBringer.DB.Model
{
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordGuildID))]
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordRoleID))]
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordGuildID), nameof(DiscordRoleID), IsUnique = true)]
    public class GuildRole
    {
        [Key]
        public int Id { get; set; }
        public ulong DiscordGuildID { get; set; }
        public ulong DiscordRoleID { get; set; }
    }
}
