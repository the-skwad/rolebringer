﻿using Microsoft.EntityFrameworkCore;

using Serilog;

namespace RoleBringer.DB
{
    public class RBDataContext : DbContext
    {
        public DbSet<Model.GuildRole> GuildRoles { get; set; }

        public static void LogDebug(DbContextOptionsBuilder options)
        {
            if (Globals.DebugDatabase)
            {
                options.LogTo(Log.Debug, Microsoft.Extensions.Logging.LogLevel.Information);
            }
        }

        public static RBDataContext GetNewContext()
        {
            switch (Globals.DatabaseType)
            {
                case DBTypes.Sqlite:
                default:
                    return new SqliteContext();
            }
        }
    }
}
