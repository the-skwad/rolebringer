﻿using Microsoft.EntityFrameworkCore;

namespace RoleBringer.DB
{
    public class SqliteContext : RBDataContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            RBDataContext.LogDebug(options);
            options
                .UseSqlite(Globals.ConnectionString, opts => opts.CommandTimeout(300))
                .UseSnakeCaseNamingConvention()
                .UseLazyLoadingProxies();
        }
    }
}
